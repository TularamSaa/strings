function str4(obj1) {
  let arr = [];
  for (let value in obj1) {
    let word = obj1[value];
    let allWordsInLowerCase = word.toLowerCase();
    arr.push(allWordsInLowerCase);
  }
  for (let i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
  }
  return arr.join(" ");
}

module.exports = str4;
