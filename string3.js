function str3(stringData, month) {
  let dateString = new Date(stringData);
  let findMonth = dateString.getMonth();
  return month[findMonth];
}

module.exports = str3;
