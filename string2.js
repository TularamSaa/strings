function str2(arr) {
  let message = arr.split(".");
  let p = message.map(Number);
  if (p.length != 4) {
    return [];
  }
  for (let i = 0; i < p.length; i++) {
    if (isNaN(p[i])) {
      return [];
    }
    if (p[i] >= 0 && p[i] <= 255) {
      continue;
    } else {
      return [];
    }
  }
  return p;
}

module.exports = str2;
