function str1(...str) {
  let result = [];
  l = str.length;
  for (let i = 0; i < l; i++) {
    str = str[i].replace(/[$,]/g, "");
    let a = isNaN(str) ? 0 : str;
    result.push(a);
  }
  return Number(result);
}

module.exports = str1;
