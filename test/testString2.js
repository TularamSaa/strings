let str2 = require("../string2");

let arr = "111.139.161.143";
let result = str2(arr);
console.log(result);

let arr2 = "111.123.1ac.123";
let result1 = str2(arr2);
console.log(result1);

let arr3 = "257.111.111.211";
let result2 = str2(arr3);
console.log(result2);

let arr4 = "257.111.111.211.122";
let result3 = str2(arr4);
console.log(result3);
